package com.hardiani_10191038.belajarfragment.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.hardiani_10191038.belajarfragment.fragment.Tab1Fragment;
import com.hardiani_10191038.belajarfragment.fragment.Tab2Fragment;

public class TabFragmentPagerAdapter extends FragmentPagerAdapter {
    String[] title = new String[]{
            "Tab 1","Tab 2"
    };

    public TabFragmentPagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = new Tab1Fragment();
                break;
            case 1:
                fragment = new Tab2Fragment();
                break;
            default:
                fragment = null;
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return title.length;
    }
}
